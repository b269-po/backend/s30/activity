// Create documents to use for the discussion
db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);


// Using aggregate method
/*
SYNTAX:
  db.collection.aggregate([
    {$match: {fieldA: valueA} },
    {$group: {_id: "$field", result: {operation}} }
  ]);
*/

db.fruits.aggregate([
  // The "match" is used to pass the documents that meet the specified condition(s) to the next pipeline/aggregation process
  {$match: {onSale: true} },
  // The "$group" is used to group elements together and field value pairs using data from the grouped elements
  {$group: {_id: "$supplier_id", total: {$sum: "$stock"} } }
]);

// Field projection with aggregation
/*
SYNTAX: 
  {$project: {field: 1/0}};
*/
db.fruits.aggregate([
  {$match: {onSale: true} },
  {$group: {_id: "$supplier_id", total: {$sum: "$stock"} } },
  {$project: {_id: 0}}
]);


// Sorting aggregated result
// MINI-ACTIVITY
// 3 minutes
db.fruits.aggregate([
  {$match: {onSale: true} },
  {$group: {_id: "$supplier_id", total: {$sum: "$stock"} } },
  {$sort: { total: -1 } }
]);


// Aggregating results based on array fields
/*
SYNTAX:
  db.collection.aggregate([
    {$unwind: field}
  ]);
*/
db.fruits.aggregate([
  // Deconstructs array field form the input documents to output each element
  {$unwind: "$origin"}
]);


// Displays fruit documents by their origin and the kinds of fruits that are supplied
db.fruits.aggregate([
  {$unwind: "$origin"},
  { $group: {_id: "$origin", kind_of_fruits: {$sum : 1} } }
]);

